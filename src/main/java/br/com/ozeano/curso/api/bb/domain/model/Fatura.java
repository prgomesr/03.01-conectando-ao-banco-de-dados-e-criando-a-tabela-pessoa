package br.com.ozeano.curso.api.bb.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class Fatura extends BaseEntity {

	private BigDecimal valor;
	private LocalDate dataVencimento;

	@Enumerated(EnumType.STRING)
	private TipoFatura tipo;

	@Enumerated(EnumType.STRING)
	private TipoPagamento tipoPagamento;

	@Enumerated(EnumType.STRING)
	private SituacaoFatura situacao;

	private String numeroDocumento;
	private String nossoNumero;

	@ManyToOne
	@JoinColumn(name = "conta_id")
	private Conta conta;

	@ManyToOne
	@JoinColumn(name = "convenio_id")
	private Convenio convenio;

	@ManyToOne
	@JoinColumn(name = "pessoa_id")
	private Pessoa pessoa;

}
